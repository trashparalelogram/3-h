from sqlalchemy import create_engine, Column, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DATABASE_URL = "sqlite:///./db/your_database.db"
Base = declarative_base()

# Database connection
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Model definition
class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, index=True)
    category = Column(String, index=True)
    firstname = Column(String)
    lastname = Column(String)
    email = Column(String, index=True)
    gender = Column(String)
    birthDate = Column(Date)

# Create the database tables
Base.metadata.create_all(bind=engine)
