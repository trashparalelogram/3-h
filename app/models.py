from sqlalchemy import Column, Integer, String, Date
from .database import Base



class User(Base):
    __tablename__ = "users"
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True, index=True)
    category = Column(String, index=True)
    firstname = Column(String)
    lastname = Column(String)
    email = Column(String, unique=True, index=True)
    gender = Column(String)
    birthDate = Column(Date)


