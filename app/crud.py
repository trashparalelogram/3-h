import csv
from datetime import datetime, date, timedelta
from typing import Optional

from sqlalchemy.orm import Session
from .models import User

def load_csv_data_to_db(db: Session, csv_file_path: str):
    with open(csv_file_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            # Convert the string to a date object
            birth_date = datetime.strptime(row['birthDate'], '%Y-%m-%d').date()

            user = User(
                category=row['category'],
                firstname=row['firstname'],
                lastname=row['lastname'],
                email=row['email'],
                gender=row['gender'],
                birthDate=birth_date
            )
            db.add(user)
        db.commit()

def get_filtered_users(db: Session, category: Optional[str] = None, gender: Optional[str] = None, birth_date: Optional[date] = None, min_age: Optional[int] = None, max_age: Optional[int] = None):
    query = db.query(User)

    if category:
        query = query.filter(User.category == category)
    if gender:
        query = query.filter(User.gender == gender)
    if birth_date:
        query = query.filter(User.birthDate == birth_date)
    if min_age:
        # Calculate the maximum birth date allowed for the minimum age
        max_birth_date = date.today() - timedelta(days=min_age*365.25)
        query = query.filter(User.birthDate <= max_birth_date)
    if max_age:
        # Calculate the minimum birth date allowed for the maximum age
        min_birth_date = date.today() - timedelta(days=(max_age+1)*365.25)
        query = query.filter(User.birthDate >= min_birth_date)
    return query.all()

