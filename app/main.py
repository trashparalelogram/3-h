# Standard library imports
import csv
import io
import os
import shutil
from datetime import date
from typing import List, Optional

# Third-party library imports
from fastapi import FastAPI, Depends, File, UploadFile, Query
from fastapi.responses import HTMLResponse, StreamingResponse, FileResponse
from sqlalchemy.orm import Session


# Application-specific imports
from app import crud, models, database
from app.crud import get_filtered_users
from app.database import engine
from app.process_file import remove_duplicates
from app.schemas import User
# Create database tables
models.Base.metadata.create_all(bind=engine)

app = FastAPI()
# Dependency to get the database session
def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()



@app.get("/users/", response_model=List[User])
def read_users(
    db: Session = Depends(get_db),
    category: Optional[str] = None,
    gender: Optional[str] = None,
    birth_date: Optional[date] = None,
    min_age: Optional[int] = None,
    max_age: Optional[int] = None
):
    # Your filtering logic here
    return crud.get_filtered_users(db, category, gender, birth_date, min_age, max_age)

@app.get("/uploadfile/", response_class=HTMLResponse)
async def upload_form():
    return FileResponse('app/file_upload.html')



@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile = File(...), db: Session = Depends(get_db)):
    UPLOAD_DIR = "uploads"
    # Ensure the upload directory exists
    os.makedirs(UPLOAD_DIR, exist_ok=True)

    # Define the full path for the uploaded file within the upload directory
    file_path = os.path.join(UPLOAD_DIR, file.filename)

    # Save the uploaded file to the upload directory
    with open(file_path, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)

    # Remove duplicates from the uploaded file (if this is still required)
    remove_duplicates(file_path)

    # Process the cleaned file to load its contents into the database
    crud.load_csv_data_to_db(db, file_path)

    # Optionally, remove the file after processing if temporary storage is preferred
    # os.remove(file_path)

    return {"filename": file.filename}



@app.get("/users/export/")
def export_users(
    db: Session = Depends(get_db),
    category: Optional[str] = Query(None, description="Filter users by category"),
    gender: Optional[str] = Query(None, description="Filter users by gender"),
    birth_date: Optional[date] = Query(None, description="Filter users by birth date"),
    min_age: Optional[int] = Query(None, description="Minimum age of users"),
    max_age: Optional[int] = Query(None, description="Maximum age of users"),
):
    """
    Export users data in CSV format according to the specified filters.
    """

    users = get_filtered_users(db, category, gender, birth_date, min_age, max_age)

    # Create a CSV in memory
    output = io.StringIO()
    writer = csv.writer(output)

    # Write header row
    writer.writerow(["id", "firstname", "lastname", "email", "category", "gender", "birthDate"])

    # Write user data rows
    for user in users:
        writer.writerow([user.id, user.firstname, user.lastname, user.email, user.category, user.gender, user.birthDate])

    # Reset the buffer position
    output.seek(0)

    # Return CSV as a streaming response
    response = StreamingResponse(iter([output.getvalue()]), media_type="text/csv")
    response.headers["Content-Disposition"] = "attachment; filename=export.csv"
    return response