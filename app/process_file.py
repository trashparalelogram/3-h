import csv
import os

from app import crud, database

def process_file(file_path: str):
    db = database.SessionLocal()
    try:
        crud.load_csv_data_to_db(db, file_path)
    finally:
        db.close()


def remove_duplicates(input_file):
    rows_by_email = {}  # To store the first occurrence of each row by email

    # Read input CSV file and store the first occurrence of each row by its email
    with open(input_file, 'r', newline='') as infile:
        reader = csv.reader(infile)
        header = next(reader)  # Read the header row

        email_index = header.index("email")  # Get the index of the Email column

        for row in reader:
            email = row[email_index]  # Extract email from the row using its index
            if email not in rows_by_email:
                rows_by_email[email] = row  # Store the row if it's the first occurrence

    # Write unique rows (including the first occurrence of duplicates) to temporary CSV file
    temp_output_file = input_file + '_temp'
    with open(temp_output_file, 'w', newline='') as outfile:
        writer = csv.writer(outfile)
        writer.writerow(header)  # Write the header row
        for row in rows_by_email.values():  # Write each unique row
            writer.writerow(row)

    # Replace input CSV file with the temporary file
    os.replace(temp_output_file, input_file)


