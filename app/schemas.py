from pydantic import BaseModel
from datetime import date

# UserBase schema includes common attributes that are used in read and write operations
class User(BaseModel):
    category: str
    firstname: str
    lastname: str
    email: str
    gender: str
    birthDate: date

    class Config:
        from_attributes = True
