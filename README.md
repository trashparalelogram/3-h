Here's a consolidated README for your FastAPI project, ready for you to copy and use:

```markdown
# FastAPI CSV User Management Service

This project is a FastAPI application designed to manage user data stored in CSV files. It allows for uploading CSV files with user data, processing and storing this data in a SQLite database, and providing API endpoints to filter and export user data.

## Features

- **CSV Upload**: Users can upload CSV files containing user data.
- **Data Processing**: The application processes CSV files, removes duplicates, and stores user data in a database.
- **API Endpoints**: Provides endpoints to filter users by various criteria and export filtered data to CSV.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Python 3.8+
- Docker (optional, for containerized deployment)

### Installing

Clone the repository:

```bash
git clone https://gitlab.com/test8392909/test_task_tg.git
cd test_task_tg
```

Install the required Python packages:

```bash
pip install -r requirements.txt
```

### Running the Application

Run the FastAPI application:

```bash
uvicorn app.main:app --reload
```

The application will be available at `http://127.0.0.1:8000`.

### Using Docker (Optional)

To build and run the application using Docker:

```bash
docker build -t fastapi-csv-user-service .
docker run -d -p 8000:8000 fastapi-csv-user-service
```

## Usage

### Upload CSV File

Navigate to `http://127.0.0.1:8000/uploadfile/` in your browser to upload a CSV file with user data.

## API Endpoints

### Get Users

- **Endpoint**: `GET /users/`
- **Description**: Retrieve users with optional filtering.
- **Query Parameters**:
  - `category` (optional): Filter users by their favorite category (e.g., `books`, `electronics`).
  - `gender` (optional): Filter users by gender (e.g., `male`, `female`).
  - `birth_date` (optional): Filter users by their exact birth date (format: `YYYY-MM-DD`).
  - `min_age` (optional): Filter users by minimum age.
  - `max_age` (optional): Filter users by maximum age.

Example request: `GET /users/?gender=female&min_age=18&max_age=30`

### Export Users

- **Endpoint**: `GET /users/export/`
- **Description**: Export filtered users to a CSV file, based on the applied filters.
- **Query Parameters**:
  - The same query parameters as for the `GET /users/` endpoint can be used here to filter which users are included in the exported CSV file.

Example request: `GET /users/export/?category=books&min_age=20`.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING_LINK) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE_LINK) file for details.
```


